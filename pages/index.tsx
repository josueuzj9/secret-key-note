import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { useState, useEffect } from 'react';
import fire from '../config/firebase';
import Signup from '../components/SignUp';
import Link from 'next/link';

export default function Home() {

  const [notification, setNotification] = useState('');
  const [loggedIn, setLoggedIn] = useState(false);

  fire.auth()
    .onAuthStateChanged((user) => {
      if (user) {
        setLoggedIn(true)
      } else {
        setLoggedIn(false)
      }
    })

  const handleLogout = () => {
    fire.auth()
      .signOut()
      .then(() => {
        setNotification('Logged out')
        setTimeout(() => {
          setNotification('')
        }, 5000)
      });
  }
  return (
    <>
      <h1>Welcome</h1>
      {notification}
      {!loggedIn
        ?
        <div>
          <Link href="/signup">
            <a>Register</a>
          </Link> |
          <Link href="/signin">
            <a> Login</a>
          </Link>
        </div>
        :
        <button onClick={handleLogout}>Logout</button>
      }
      <ul>
        <li>
          <Link href="#">
            <a>Dashboard</a>
          </Link>
        </li>
      </ul>
      {/* {loggedIn && <CreateNote /> */}
    </>
  )
}

import React from 'react';
class Dashboard extends React.Component {
    render() {
        return (
            <div>
                <h1>Dashboard  Page</h1>
                <p>You can't go into this page if you are not authenticated.</p>
            </div>
        )
    }
}
export default Dashboard;
import firebase from 'firebase/app';
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyDV22k06hNeMmg--295y77fY71d2q87IQk",
    authDomain: "secret-key-notes.firebaseapp.com",
    databaseURL: "https://secret-key-notes.firebaseio.com",
    projectId: "secret-key-notes",
    storageBucket: "secret-key-notes.appspot.com",
    messagingSenderId: "354568970",
    appId: "1:354568970:web:912e5e35d38d7955ccc8f3",
};

try {
    firebase.initializeApp(firebaseConfig);
} catch (err) {
    if (!/already exists/.test(err.message)) {
        console.error('Firebase initialization error', err.stack)
    }
}
const fire = firebase;
export default fire;
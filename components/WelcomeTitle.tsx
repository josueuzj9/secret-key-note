import Typography from '@material-ui/core/Typography';
const WelcomeTitle = () => {
    return (
        <Typography component="h1" variant="h3" style={{ fontSize: 24 }}>
            Welcome to Secret key Notes
        </Typography>
    )
}

export default WelcomeTitle
